package com.neuedu.test1028;

public class Test_03_DuanLu {
	public static void main(String[] args) {
		//演示逻辑运算符的短路功能,位运算符不能短路
		//对于&&来说,如果左侧是false,则不会执行右侧表达式
		//对于||来说,如果左侧是true,就不会执行右侧表达式
		int x = 5;
		int y = 6;
		boolean flag = x>6 && y++>5;
		System.out.println("y="+y);//短路了,右侧的y++>5没有执行
		
		flag = x>6 & y++>5;
		System.out.println("y="+y);//没有短路,右侧y++>5执行
		
		x=5;
		y=6;
		flag = x>2 || y++>5;
		System.out.println("y="+y);//短路,又没执行
		
		flag = x>2 | y++>5;
		System.out.println("y="+y);//没有短路,右侧y++>5执行
		
		//三元运算符
		x=5;
		y=6;
		x=x++<y--?++x:--y;
		System.out.println(x);
	}
}
