package com.neuedu.test1028;

public class Test_04_ZhuanHuan {
	public static void main(String[] args) {
		int a = 8;
		long b = a;//把int类型的a自动转换为long类型,赋值给b
		//把浮点数3.14转换为int类型,会丢失小数点后的值再赋值给C
		double d = 3.14;
		int c = (int)d;
		System.out.println(c);
		
		
		byte e = 5;
		byte f = 6;
		byte g =(byte)(e + f);
		
		char h = '0';
		int i = h+5;//h先被转为int类型的48,再与5相加
		System.out.println(i);
	}
}
