package com.neuedu.test1028;

public class Test_01_Primitive {
	public static void main(String[] args) {
		
		//1.声明四种整形变量,并初始化
		byte a=10;
		short b=20;
		int c=30;
		long d=40L;
		
		//2.使用上面的变量
		System.out.println("a="+a);
		System.out.println("b="+b);
		System.out.println("c="+c);
		System.out.println("d="+d);
		
		//3.试验进制不同的写法
		int e=50;//十进制,默认进制
		int f=0x2a;//16进制
		int g=027;//8进制
		int h=0b10_10;//2进制
		
		//4.输出变量
		System.out.println("e="+e);
		System.out.println("f="+f);
		System.out.println("g="+g);
		System.out.println("h="+h);
		System.out.println();
		
		//5.浮点型的使用
		float i=1.5F;
		double j=3.14D;
		System.out.println("i="+i);
		System.out.println("j="+j);
		
		//6.字符型的使用
		char k='a';
		char l='中';
		char m='\u4e2d';
		char n='\t';
		
		System.out.println("k="+k);
		System.out.println("l="+l);
		System.out.println("m="+m);
		System.out.println("n="+n+'-');
		boolean o=true;
		System.out.println("o="+o);
	}
}
