package com.neuedu.test1028;

public class HuHuan_20201028_homework3 {
	public static void main(String[] args) {
		//定义a与b
		int a = 1;
		int b = 2;
		
		//a与b互换
		int c=a;
		a=b;
		b=c;
		
		//输出互换前与互换后的a与b
		System.out.println("互换前:a="+b+",b="+a+";互换后:a="+a+",b="+b);
	}
}
