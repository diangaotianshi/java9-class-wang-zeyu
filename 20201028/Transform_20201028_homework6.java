package com.neuedu.test1028;

public class Transform_20201028_homework6 {
	public static void main(String[] args) {
		//给定一个大写字母
		char a = 'H';
		
		//将其转换为小写字母
		char b = (char)(a+32);
		
		//输出转换前后的字母
		System.out.println("大写字母"+a+"转换为小写字母为:"+b);
	}
}
