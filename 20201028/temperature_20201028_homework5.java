package com.neuedu.test1028;

public class temperature_20201028_homework5 {
	public static void main(String[] args) {
		//给定一个摄氏度和一个华氏度
		int ca = 30;
		int fa = 100;
		
		//将摄氏度与华氏度互相转换
		double caafter = ca*9/5+32;
		double faafter = (fa-32)*5/9;
		
		//输出转换前后的两种温度
		System.out.println("摄氏度"+ca+"°转化为华氏度数值为:"+caafter);
		System.out.println("华氏度"+fa+"°F转化为摄氏度数值为:"+faafter);
	}
}
