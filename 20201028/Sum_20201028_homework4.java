package com.neuedu.test1028;

public class Sum_20201028_homework4 {
	public static void main(String[] args) {
		//给定一个三位整数
		int a = 996;
		
		//计算该整数每一位的值
		int b = a%10;
		int c = a/10%10;
		int d = a/100%10;
		
		//计算并输出三位的和
		int sum = b + c + d;
		System.out.println(a+"各位数的和为:"+sum);
	}
}
