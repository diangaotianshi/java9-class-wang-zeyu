package com.neuedu.test1030;

import java.util.Scanner;

public class Test_05_Even {
	public static void main(String[] args) {
		//使用scanner获取用户输入的值
		Scanner sc = new Scanner(System.in);
		//sc.next()可以获取控制台输入的字符串值
		//sc.nextint()可以获取控制台输入的整数值
		
		int x = sc.nextInt();
		
		sc.close();
		
		System.out.print(x);
		if(x%2==0) {
			System.out.println("为偶数!");
		}else {
			System.out.println("为奇数!");
		}
	}
}
