package com.neuedu.test1030;

public class Test_09_Switch {
	public static void main(String[] args) {
		int n=2;
	int result;
	
	switch(n+1) {
	case 1:
		System.out.println("block1");
		result=n;
		break;
	case 2:
		System.out.println("block2");
		result=n*n;
		break;
	case 3:
		System.out.println("block3");
		result=n*n*n;
		break;
	default:
		result=0;
	}
	System.out.println(result);
	}
	
}
