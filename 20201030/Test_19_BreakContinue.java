package com.neuedu.test1030;

import java.util.Scanner;

public class Test_19_BreakContinue {
	public static void main(String[] args) {
		int sum=0;
		for(;;) {
			if(sum>=666) {
				break;
			}
			sum+=20;
		}
		System.out.println("sum="+sum);
		
		//获取用户输入,直到0为止
		Scanner sc = new Scanner(System.in);
		while(true) {
			System.out.println("请输入0然后回车,结束循环");
			int i = sc.nextInt();
			System.out.println("i="+i);
			if(i==0) {
				break;
			}
		}
		sc.close();
	}
}
