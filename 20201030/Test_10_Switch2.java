package com.neuedu.test1030;

public class Test_10_Switch2 {
	public static void main(String[] args) {
		int x = 5 ;
		switch(x) {
		case 1:
			System.out.println("case 1");
			x++;
		case 5:
			System.out.println("case 5");
			x++;
		case 6:
			System.out.println("case 6");
			x++;
		case 7:
			System.out.println("case 7");
			x++;
		default:
			System.out.println("default");
			x++;
		}
		System.out.println(x);
	}
}
