package com.neuedu.test1030;

import java.util.Scanner;

public class Test_11_Scanner {
	public static void main(String[] args) {
		//创建Scanner对象
		Scanner sc = new Scanner(System.in);
		
		//1.获取整数
		System.out.println("请输入整数");
		int i = sc.nextInt();
		System.out.println("你输入的是整数"+i);
		
		//2.获取浮点数
		System.out.println("请输入浮点数");
		double d = sc.nextDouble();
		System.out.println("你输入的是浮点数"+d);
		
		//如果在控制台输出的字符串中没有空格,则sc.next()方法与sc.nextline()方法相同
		//如果有空格的话,sc.next()会按空格分隔读取
		//3.获取字符串(遇到空格表示结束)
		System.out.println("请输入两个字符串,使用空格隔开");
		String s1 = sc.next();//只能取得空格前面的字符串
		String s2 = sc.next();//只能取得空格后面的字符串
		sc.nextLine();//在有空格的情况下,需要特殊读取一下换行符
		System.out.println("你输入的是字符串"+s1+","+s2);
		
		//4.获取字符串(遇到回车表示结束)
		System.out.println("请输入三个字符串,使用空格隔开");
		String s3 = sc.nextLine();//只能取得空格前面的字符串
		System.out.println("你输入的是字符串"+s3);
		sc.close();
	}
}
