package com.neuedu.test1030;

public class Test_20_Continue {
	public static void main(String[] args) {
		int sum=0;
		for(int i=1;i<50;i++) {
			if(i%2==0) {
				continue;
			}
			sum+=i;
		}
		System.out.println("sum="+sum);
	}
}
