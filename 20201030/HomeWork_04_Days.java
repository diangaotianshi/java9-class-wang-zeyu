package com.neuedu.test1030;

import java.util.Random;

public class HomeWork_04_Days {
	public static void main(String[] args) {
		//获得一个随机的月份和一个随机的天数
		Random ran = new Random();
		int m = ran.nextInt(12)+1;
		int d;
		if(m==2) {
			d = ran.nextInt(28)+1;
		}else if(m==4||m==6||m==9||m==11) {
			d = ran.nextInt(30)+1;
		}else {
			d = ran.nextInt(31)+1;
		}
		
		//计算并输出该日是本年的第几天
		int day;
		switch (m) {
		case 1:
			day=d;
			System.out.println(m+"月"+d+"日是本年的第"+day+"天!");
			break;
		case 2:
			day=31+d;
			System.out.println(m+"月"+d+"日是本年的第"+day+"天!");
			break;
		case 3:
			day=31+28+d;
			System.out.println(m+"月"+d+"日是本年的第"+day+"天!");
			break;
		case 4:
			day=31+28+31+d;
			System.out.println(m+"月"+d+"日是本年的第"+day+"天!");
			break;
		case 5:
			day=31+28+31+30+d;
			System.out.println(m+"月"+d+"日是本年的第"+day+"天!");
			break;
		case 6:
			day=31+28+31+30+31+d;
			System.out.println(m+"月"+d+"日是本年的第"+day+"天!");
			break;
		case 7:
			day=31+28+31+30+31+30+d;
			System.out.println(m+"月"+d+"日是本年的第"+day+"天!");
			break;
		case 8:
			day=31+28+31+30+31+30+31+d;
			System.out.println(m+"月"+d+"日是本年的第"+day+"天!");
			break;
		case 9:
			day=31+28+31+30+31+30+31+31+d;
			System.out.println(m+"月"+d+"日是本年的第"+day+"天!");
			break;
		case 10:
			day=31+28+31+30+31+30+31+31+30+d;
			System.out.println(m+"月"+d+"日是本年的第"+day+"天!");
			break;
		case 11:
			day=31+28+31+30+31+30+31+31+30+31+d;
			System.out.println(m+"月"+d+"日是本年的第"+day+"天!");
			break;
		case 12:
			day=31+28+31+30+31+30+31+31+30+31+30+d;
			System.out.println(m+"月"+d+"日是本年的第"+day+"天!");
			break;
		

		default:
			System.out.println("系统出错啦,请重新运行!");
			break;
		}
		
		
	}
}
