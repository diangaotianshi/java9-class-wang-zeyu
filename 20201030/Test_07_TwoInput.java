package com.neuedu.test1030;

import java.util.Scanner;

public class Test_07_TwoInput {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入第一个数");
		int x =sc.nextInt();
		
		System.out.println("请输入第二个数");
		int y =sc.nextInt();
		
		sc.close();
		
		if(x>y) {
			System.out.println(x+">"+y);
		}else {
			System.out.println(x+"<"+y);
		}
	}
}
