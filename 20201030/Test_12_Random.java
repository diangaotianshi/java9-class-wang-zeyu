package com.neuedu.test1030;

import java.util.Random;

public class Test_12_Random {
	public static void main(String[] args) {
		//获取Random对象
		Random ran = new Random();
		
		//取0-9之间的随机数
		for(int i=0;i<5;i++) {
			int j  = ran.nextInt(10);
			System.out.println("j="+j);
		}
		System.out.println();
		
		//取1-100之间的随机数
		for(int i=0;i<5;i++) {
			int j  = ran.nextInt(100)+1;
			System.out.println("j="+j);
		}
		
	}
}
