package com.neuedu.test1030;

public class Test_16_DoWhile {
	public static void main(String[] args) {
		int x=3;
		do {
			System.out.println("x="+x);
			x++;
		}while(x<3);//注意,结尾分号必须有
		System.out.println();
		
		int i = 1;
		int sum = 0;
		do {
			sum+=i;
			i++;
		}while(i<=100);
		System.out.println("sum="+sum);
	}
}
