package com.neuedu.test1031;

import java.util.Scanner;

public class HomeWork_05_Sum {
	public static void main(String[] args) {
		//提示从控制台获得用户输入的5个正整数
		Scanner sc = new Scanner(System.in);
		System.out.println("请一次输入一件商品价格,并按回车确认:");
		
		//循环获得5件商品价格,并计算总价
		int sum=0;
		for(int i=1;i<6;i++) {
			int j=sc.nextInt();
			sum+=j;
			System.out.println("第"+i+"件商品价格为"+j+"元");
		}
		
		//循环5次后输出总价
		System.out.println("您选择的商品总价为:"+sum+"元");
		sc.close();
	}
}
