package com.neuedu.test1030;

import java.util.Scanner;

public class HomeWork_01_Month {
	public static void main(String[] args) {
		//从控制台获得一个月份值
		Scanner sc =new Scanner(System.in);
		System.out.println("请输入一个月份,并点击回车查看该月份的天数:");
		
		//求得该月份的天数,并输出该月份及结果
		int m = sc.nextInt();
		sc.close();
		switch (m) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			System.out.println(m+"月有31天!");
			break;
			
		case 4:
		case 6:
		case 9:
		case 11:
			System.out.println(m+"月有30天!");
			break;
			
		case 2:
			System.out.println(m+"月有28天!");
			break;

		default:
			System.out.println("您的输入有误,请重新运行并输入!");
			break;
		}
	}
}
