package com.neuedu.test1030;

import java.util.Random;

public class HomeWork_03_MonthAgain {
	public static void main(String[] args) {
		//随机产生一个月份值
		Random ran = new Random();
		
		//求得该月份的天数,并输出该月份及结果
		int m = ran.nextInt(12)+1;
		switch (m) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			System.out.println(m+"月有31天!");
			break;
			
		case 4:
		case 6:
		case 9:
		case 11:
			System.out.println(m+"月有30天!");
			break;
			
		case 2:
			System.out.println(m+"月有28天!");
			break;

		default:
			System.out.println("系统出错啦,请重新运行!");
			break;
		}
	}
}
