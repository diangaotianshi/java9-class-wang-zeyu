package com.neuedu.test1030;

import java.util.Scanner;

public class Test_08_Year {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入一个年份:");
		
		int x = sc.nextInt();
		sc.close();
		
		if(x%400==0) {
			System.out.println(x+"是闰年!");
		}else if(x%100==0&&x%400!=0) {
			System.out.println(x+"是平年!");
		}else if(x%4==0&&x%100!=0) {
			System.out.println(x+"是闰年!");
		}else {
			System.out.println(x+"是平年!");
		}
		
	}
}
